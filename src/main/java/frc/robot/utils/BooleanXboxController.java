package frc.robot.utils;

import edu.wpi.first.wpilibj.XboxController;

public class BooleanXboxController extends XboxController {
    private double m_triggerThreshold = 0.5;
    
    public BooleanXboxController(int port) {
        super(port);
    }

    public void setTriggerThreshold(double thresh) {
        m_triggerThreshold = thresh;
    }

    public boolean getPOVUpButton() {
        return getPOV() == 0;
    }

    public boolean getPOVRightButton() {
        return getPOV() == 90;
    }

    public boolean getPOVLeftButton() {
        return getPOV() == 270;
    }

    public boolean getPOVDownButton() {
        return getPOV() == 180;
    }

    public boolean getRightTriggerPressed() {
        return getRightTriggerAxis() >= m_triggerThreshold;
    }

    public boolean getLeftTriggerPressed() {
        return getLeftTriggerAxis() >= m_triggerThreshold;
    }

}
