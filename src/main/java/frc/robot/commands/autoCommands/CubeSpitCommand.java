package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.endEffectorCommands.RollOutCommand;
import frc.robot.subsystems.EndEffectorSubsystem;

public class CubeSpitCommand extends ParallelRaceGroup {
    public CubeSpitCommand(EndEffectorSubsystem endEffectorSubsystem) {
        addCommands(new RollOutCommand(endEffectorSubsystem), new WaitCommand(0.5));
    }
}
