package frc.robot.commands.autoCommands;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import com.pathplanner.lib.PathConstraints;
import com.pathplanner.lib.PathPlanner;
import com.pathplanner.lib.PathPlannerTrajectory;
import com.pathplanner.lib.auto.PIDConstants;
import com.pathplanner.lib.auto.SwerveAutoBuilder;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.DrivetrainSubsystem;

public class PathPlannerCommands {

    private final DrivetrainSubsystem m_drivetrainSubsystem;
    HashMap<String, Command> m_events = new HashMap<>();

    public PathPlannerCommands(DrivetrainSubsystem drivetrainSubsystem, HashMap<String, Command> events) {
        m_drivetrainSubsystem = drivetrainSubsystem;
        // create events
        m_events = events;
        m_events.put("test_delay", 
            new PrintCommand("Start delay!")
        );
    }
    
    private Command CreatePathPlannerCommand(
        String filename,
        double maxVelocity,
        double maxAcceleration
    ) {
        List<PathPlannerTrajectory> pathGroup = PathPlanner.loadPathGroup(
            filename,
            new PathConstraints(maxVelocity, maxAcceleration)
        );
        SwerveAutoBuilder autoBuilder = new SwerveAutoBuilder(
            m_drivetrainSubsystem.getPose,
            m_drivetrainSubsystem.resetPose,
            m_drivetrainSubsystem.m_kinematics,
            new PIDConstants(5.0, 0.0, 0.0),
            new PIDConstants(1.5, 0.0, 0.0),
            m_drivetrainSubsystem.setModuleStates,
            m_events,
            true,
            m_drivetrainSubsystem
        );
        return autoBuilder.fullAuto(pathGroup);
    }

    public Map<String, Command> createPathPlannerCommands() {
        // create Path Planner commands
        System.out.println("************** Creating Commands **************");
        System.out.println("Deploy directory: "+Filesystem.getDeployDirectory());
        Map<String, Command> pp_commands = new HashMap<>();
        try {
            try (Stream<Path> paths = Files.walk(Paths.get(Filesystem.getDeployDirectory()+"/pathplanner"))) {
                paths
                .filter(Files::isRegularFile)
                .forEach((path) -> {
                    String filename = path.getFileName().toString().replaceFirst("[.][^.]+$", "");
                    System.out.println("Create Command "+filename);
                    pp_commands.put(
                        filename, 
                        CreatePathPlannerCommand(
                            filename, 
                            filename.equals("Comp - Far Triple Score Balance.path") ? 3.0 : 2.0,
                            filename.equals("Comp - Far Triple Score Balance.path") ? 2.25 : 2.0 // acc
                        )
                    );
                });
            }      
        } catch (Exception e) {System.out.println(e);} 
        return pp_commands;
    } 
}
