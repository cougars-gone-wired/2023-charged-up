package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.ExtenderCommands.RetractCommand;
import frc.robot.commands.shoulderCommands.RotateToCubeSpitCommand;
import frc.robot.subsystems.EndEffectorSubsystem;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class ScoreLowCommand extends SequentialCommandGroup {
    public ScoreLowCommand(EndEffectorSubsystem endEffectorSubsystem, ShoulderSubsystem shoulderSubsystem, ExtensionSubsystem extensionSubsystem) {
        addCommands(new RetractCommand(extensionSubsystem), new RotateToCubeSpitCommand(shoulderSubsystem), new CubeSpitCommand(endEffectorSubsystem));
    }
    
}
