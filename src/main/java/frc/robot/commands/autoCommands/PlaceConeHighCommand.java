package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.ExtenderCommands.MoveToPositionCommand;
import frc.robot.commands.ExtenderCommands.RetractCommand;
import frc.robot.commands.endEffectorCommands.RollOutCommand;
import frc.robot.commands.shoulderCommands.RotateToConePlaceCommand;
import frc.robot.subsystems.EndEffectorSubsystem;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class PlaceConeHighCommand extends SequentialCommandGroup {
    public PlaceConeHighCommand(ShoulderSubsystem shoulderSubsystem, ExtensionSubsystem extensionSubsystem, EndEffectorSubsystem endEffectorSubsystem) {
        addCommands(new RotateToConePlaceCommand(shoulderSubsystem).alongWith(new RetractCommand(extensionSubsystem)), new MoveToPositionCommand(extensionSubsystem, Constants.ExtensionConstants.CONE_UPPER_NODE_POS), new ConeSpitCommand(endEffectorSubsystem));
    }
}
