package frc.robot.commands.autoCommands;

import edu.wpi.first.hal.AllianceStationID;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.DriveAutoLineCommandGroup;
import frc.robot.commands.driveCommands.ZeroCommand;
import frc.robot.subsystems.DrivetrainSubsystem;

public class TaxiAutoCommand extends SequentialCommandGroup{

    public TaxiAutoCommand(DrivetrainSubsystem driveTrain) {
        
        addCommands(new DriveAutoLineCommandGroup(driveTrain, 4.3, 0, 0, 1.5 * Constants.TIME_MULT));
    }
}
