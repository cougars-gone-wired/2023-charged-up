package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.subsystems.EndEffectorSubsystem;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class TaxiScoreAuto extends SequentialCommandGroup {
    public TaxiScoreAuto(EndEffectorSubsystem effectorSubsystem, ShoulderSubsystem shoulderSubsystem, ExtensionSubsystem extensionSubsystem, DrivetrainSubsystem drivetrainSubsystem) {
        addCommands(new ScoreLowCommand(effectorSubsystem, shoulderSubsystem, extensionSubsystem), new QuickNestCommand(extensionSubsystem, shoulderSubsystem).alongWith(new TaxiAutoCommand(drivetrainSubsystem)));
    }
}
