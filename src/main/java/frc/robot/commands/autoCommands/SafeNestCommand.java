package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.ExtenderCommands.MoveToPositionCommand;
import frc.robot.commands.ExtenderCommands.RetractCommand;
import frc.robot.commands.shoulderCommands.RotateToNestedCommand;
import frc.robot.commands.shoulderCommands.RotateToPositionCommand;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class SafeNestCommand extends SequentialCommandGroup {
    public SafeNestCommand(ExtensionSubsystem extensionSubsystem, ShoulderSubsystem shoulderSubsystem) {
        // If the intake is extended past bumpers and against them, then we cannot safely retract the arm without rotating to a safe angle
        addCommands(
            new ConditionalCommand(
                new RotateToPositionCommand(shoulderSubsystem, Constants.ShoulderConstants.CUBE_INTAKE_BUMPER_ENCODER_ANGLE)
                    .alongWith(new MoveToPositionCommand(extensionSubsystem, Constants.ExtensionConstants.CUBE_INTAKE_BUMPER_POS)),
                new InstantCommand(), // Don't run additional commands
                () -> extensionSubsystem.getEncoderPosition() >= Constants.ExtensionConstants.MIN_EXTENSION_PAST_BUMPER_POS && 
                    shoulderSubsystem.getShoulderAngle() >= Constants.ShoulderConstants.CUBE_INTAKE_BUMPER_ENCODER_ANGLE),
            new RetractCommand(extensionSubsystem),
            new RotateToNestedCommand(shoulderSubsystem));
    }
}
