package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.ExtenderCommands.MoveToPositionCommand;
import frc.robot.commands.ExtenderCommands.RetractCommand;
import frc.robot.commands.shoulderCommands.RotateToPositionCommand;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class CubeIntakePosCommand extends SequentialCommandGroup {
    public CubeIntakePosCommand(ExtensionSubsystem extensionSubsystem, ShoulderSubsystem shoulderSubsystem) {
        addCommands(new RetractCommand(extensionSubsystem), // Make sure the winch gets zeroed and is in a known position --> may replace with a MoveToPosition(0);
                    new RotateToPositionCommand(shoulderSubsystem, Constants.ShoulderConstants.CUBE_INTAKE_BUMPER_ENCODER_ANGLE), // Rotate to an angle known to be above bumpers
                    new MoveToPositionCommand(extensionSubsystem, Constants.ExtensionConstants.CUBE_INTAKE_BUMPER_POS), // Extend to a position past bumpers and intake pos before retracting into place
                    new RotateToPositionCommand(shoulderSubsystem, Constants.ShoulderConstants.CUBE_INTAKE_SUCC_ENCODER_ANGLE), // Rotate to intake position angle
                    new MoveToPositionCommand(extensionSubsystem, Constants.ExtensionConstants.CUBE_INTAKE_POS)); // Extend (actually retract) to intaking position nested against bumpers
    }
}
