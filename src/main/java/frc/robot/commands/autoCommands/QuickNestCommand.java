package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import frc.robot.commands.ExtenderCommands.RetractCommand;
import frc.robot.commands.shoulderCommands.RotateToNestedCommand;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class QuickNestCommand extends ParallelCommandGroup {
    QuickNestCommand(ExtensionSubsystem extensionSubsystem, ShoulderSubsystem shoulderSubsystem) {
        addCommands(new RetractCommand(extensionSubsystem), new RotateToNestedCommand(shoulderSubsystem));
    }
}
