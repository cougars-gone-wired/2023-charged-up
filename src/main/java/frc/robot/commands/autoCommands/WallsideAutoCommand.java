package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.BalanceCommand;
import frc.robot.commands.DriveAutoLineCommandGroup;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.Constants;

public class WallsideAutoCommand extends SequentialCommandGroup{
    
    public WallsideAutoCommand(DrivetrainSubsystem driveTrain, String side) {
        double strafeMultiplier;
        
        if(side.equals("red"))
            strafeMultiplier = -1;
        else
            strafeMultiplier = 1;
        
        addCommands(new DriveAutoLineCommandGroup(driveTrain, 4.6, 0, 0, 2.5 * Constants.TIME_MULT), new DriveAutoLineCommandGroup(driveTrain, 0, strafeMultiplier * 0.5, 0, 0.5 * Constants.TIME_MULT), new DriveAutoLineCommandGroup(driveTrain, -2.5, strafeMultiplier * 2, 0, 2 * Constants.TIME_MULT), new BalanceCommand(driveTrain));
    }
}
