package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.subsystems.EndEffectorSubsystem;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;

public class BalanceScoreAuto extends SequentialCommandGroup {
    public BalanceScoreAuto(EndEffectorSubsystem endEffectorSubsystem, ShoulderSubsystem shoulderSubsystem, ExtensionSubsystem extensionSubsystem, DrivetrainSubsystem drivetrainSubsystem, String alliance) {
        addCommands(new ScoreLowCommand(endEffectorSubsystem, shoulderSubsystem, extensionSubsystem), new QuickNestCommand(extensionSubsystem, shoulderSubsystem).alongWith(new WallsideAutoCommand(drivetrainSubsystem, alliance)));
    }
}
