package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.endEffectorCommands.RollInCommand;
import frc.robot.commands.endEffectorCommands.RollOutCommand;
import frc.robot.subsystems.EndEffectorSubsystem;

public class ConeSpitCommand extends ParallelRaceGroup {
    public ConeSpitCommand(EndEffectorSubsystem endEffectorSubsystem) {
        addCommands(new RollInCommand(endEffectorSubsystem), new WaitCommand(1));
    }
}
