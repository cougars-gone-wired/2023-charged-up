package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DrivetrainSubsystem;

public class DriveAutoLine extends CommandBase{
    private final DrivetrainSubsystem m_drivetrain;
    private double m_xDistance;
    private double m_yDistance;
    private double m_time;
    private double m_xVelocity;
    private double m_yVelocity;

    private PIDController rotationController;
    private final double rotationKp = Math.PI;
    private final double rotationKi = 0;
    private final double rotationKd = 0;

    private double rot = 1;
    
    public DriveAutoLine(DrivetrainSubsystem drivetrain, double x, double y, double r, double t) {
        m_drivetrain = drivetrain;
        m_xDistance = x;
        m_yDistance = y;
        m_time = t;
        
        rotationController = new PIDController(rotationKp, rotationKi, rotationKd);
        rotationController.setSetpoint(r);
        
        addRequirements(m_drivetrain);
    }

    @Override
    public void initialize() {
        m_xVelocity = m_xDistance / m_time;
        m_yVelocity = m_yDistance / m_time;
    }

    @Override
    public void execute() {
        if(rotationController.getSetpoint() == 0) {rot = 0;}
        m_drivetrain.drive(ChassisSpeeds.fromFieldRelativeSpeeds(m_xVelocity, m_yVelocity, rot * rotationController.calculate(m_drivetrain.getRotation().getRadians()), m_drivetrain.getRotation()));
    }

    @Override
    public void end(boolean interrupted) {
        m_drivetrain.drive(new ChassisSpeeds(0,0,0));
    }
}
