package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.DrivetrainSubsystem;

public class DriveAutoLineCommandGroup extends ParallelRaceGroup{
    
    public DriveAutoLineCommandGroup(DrivetrainSubsystem drivetrain, double xDistance, double yDistance, double rDistance, double time) {
        addCommands(new DriveAutoLine(drivetrain, xDistance, yDistance, rDistance, time), new WaitCommand(time));
    }
}
