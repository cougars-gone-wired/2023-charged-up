package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.subsystems.Limelight;

public class ReflectiveAutoAlignCommandGroup extends SequentialCommandGroup{
    private final Limelight m_limelight;

    public ReflectiveAutoAlignCommandGroup(DrivetrainSubsystem driveTrain, Limelight limelight, double strafe, double setPoint) {
        m_limelight = limelight;

        addCommands(new InstantCommand(m_limelight::setPipelineReflective), new DriveAutoLineCommandGroup(driveTrain, 0, strafe, 0, 1), new ReflectiveAutoAlign(driveTrain, limelight, setPoint));
    }
}
