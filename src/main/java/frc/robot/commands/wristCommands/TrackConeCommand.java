package frc.robot.commands.wristCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.WristSubsystem;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.CvSink;
import org.opencv.core.Mat;
import frc.robot.ConeVisionPipeline;

public class TrackConeCommand extends CommandBase {
    private final ConeVisionPipeline pipeline = new ConeVisionPipeline();
    private Mat mat = new Mat();
    private CvSink m_camera;
    private WristSubsystem m_wristSubsystem;
    private double initialCamAngle;
    private Thread m_visionThread;

    public TrackConeCommand(WristSubsystem wristSubsystem) {
        m_wristSubsystem = wristSubsystem;

        m_camera = CameraServer.getVideo();

        addRequirements(m_wristSubsystem);
    }

    @Override
    public void initialize() {
        m_wristSubsystem.stopMoving();
        m_camera.grabFrame(mat);
        m_visionThread = new Thread(() -> {pipeline.process(mat);});
        m_visionThread.setDaemon(true);
        m_visionThread.start();
        // pipeline.process(mat);

        initialCamAngle = m_wristSubsystem.getAbsoluteAngle();
        m_wristSubsystem.setNotAligned();
    }

    @Override
    public void execute() {
        // m_camera.grabFrame(mat);
        // pipeline.process(mat);
        if(!m_visionThread.isAlive())
            m_wristSubsystem.rotateToAngle(pipeline.getTriangleAngle());
            // m_wristSubsystem.rotateToAngle(initialCamAngle + pipeline.getTriangleAngle());
        // m_wristSubsystem.rotateToAngle(0);
    }

    @Override
    public boolean isFinished() {
        if(!m_visionThread.isAlive()) 
            return m_wristSubsystem.getAligned();
        else
            return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_wristSubsystem.stopMoving();
        if(interrupted && m_visionThread.isAlive()) {
            // Stop the vision thread if the command was interupted and it's still running
            m_visionThread.interrupt();
        }
    }
}
