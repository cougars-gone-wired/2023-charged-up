package frc.robot.commands.wristCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.CvSource;

import org.opencv.core.Mat;
import frc.robot.ConeVisionPipeline;
import frc.robot.ConeVisionPipeline.CentroidHalfEnum;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class ConeCommand extends CommandBase {
    
    Thread coneThread;
    private Mat mat;
    private Mat lastMat;
    private final ConeVisionPipeline pipeline = new ConeVisionPipeline();

    
    private int[][] hist;
    private double[] rowVals;
    private double[] colVals;

    private ConeVisionPipeline.CentroidHalfEnum[] halves;

    private CvSource source;
    private CvSource source2;

    public ConeCommand() {
        /*
        coneThread = new Thread(()->{
            System.out.println("Thread created");
            var pipeline = new ConeVisionPipeline();
            Mat mat = new Mat();

            CameraServer.getVideo().grabFrame(mat);
            pipeline.process(mat);
        });*/
    }

    @Override
    public void initialize() {
        // coneThread.start();
        mat = new Mat();
        CameraServer.getVideo().grabFrame(mat);
        pipeline.process(mat);

        /*
        hist = pipeline.getHistogram();

        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("X");
        for(int i = 0; i < hist[0].length; i++) {
            System.out.print(i + ":" + hist[0][i] + ", ");
        }
        System.out.println();

        System.out.println("Y");
        for(int i = 0; i < hist[1].length; i++) {
            System.out.print(i + ":" + hist[1][i] + ", ");
        }
        System.out.println();
        System.out.println();
        System.out.println();

        */




        /*
        hist = pipeline.getHistogram();
        rowVals = new double[hist[0].length];
        for(int i = 0; i < rowVals.length; i++) {
            rowVals[i] = hist[0][i];
        }
        SmartDashboard.putNumberArray("RowVals", rowVals);
        
        colVals = new double[hist[1].length];
        for(int i = 0; i < colVals.length; i++) {
            colVals[i] = hist[1][i];
        }
        SmartDashboard.putNumberArray("ColVals", colVals);
        */

        source = CameraServer.putVideo("Cone", 320, 240);
        source2 = CameraServer.putVideo("Errode", 320, 240);
        pipeline.drawBoundingBox(mat);
        source.putFrame(mat);

        // halves = pipeline.getCentroidHalves();
        
        // Since this /\ should return the smaller halves i.e. the halves that the base SHOULD be on...
        // It the direction to run motors in order to orient cone should be possible to figure out
        
        // System.out.println("Halves[0]: " + halves[0].toString() + " Halves[1]: " + halves[1].toString());
        
        // y = 0 is the top of the mat, y = 239 is bottom
        // x = 0 is the left of mat, x = 319 is right

        // If ctoid is in top-half (lower half), then just close orienter and run motors "in"
        //      Would need some sort of thresh
        
        // If ctoid is in bottom-half (upper half), then motors need to run opposite in order to right the cone
        //  If ctoid is also on left-half, then run left motors "in" to pull base down and right motors "out" to push tip up
        //  else, the ctoid is on the right-half, then run the right motors "in" to pull base down and left motors "out" to push tip up
        //  Running the motors fast enough should force the cone to orient properly as the pneumatic pistons close


        // pipeline.drawBoundingBox(pipeline.cvErodeOutput());
        source2.putFrame(pipeline.cvErodeOutput());
    }

    @Override
    public void execute() {
        lastMat = mat;
        CameraServer.getVideo().grabFrame(mat);
        if(true){//!lastMat.equals(mat)) {
            pipeline.process(mat);


            // halves = pipeline.getCentroidHalves();
            // System.out.println("Halves[0]: " + halves[0].toString() + "\nHalves[1]: " + halves[1].toString());
            /*
            hist = pipeline.getHistogram();
            rowVals = new double[hist[0].length];
            for(int i = 0; i < rowVals.length; i++) {
                rowVals[i] = hist[0][i];
            }
            

            colVals = new double[hist[1].length];
            for(int i = 0; i < colVals.length; i++) {
                colVals[i] = hist[1][i];
            }
            SmartDashboard.putNumberArray("ColVals", colVals);
            */
            pipeline.drawBoundingBox(mat);
            source.putFrame(mat);
            // pipeline.drawBoundingBox(pipeline.cvErodeOutput());
            source2.putFrame(pipeline.cvErodeOutput());

            double[][] points = pipeline.getTrianglePoints();

            if(points[0] != null) {

                double p0p1 = Math.sqrt(Math.pow(points[0][0] - points[1][0], 2) + Math.pow(points[0][1] - points[1][1], 2));
                double p1p2 = Math.sqrt(Math.pow(points[1][0] - points[2][0], 2) + Math.pow(points[1][1] - points[2][1], 2));
                double p2p0 = Math.sqrt(Math.pow(points[2][0] - points[0][0], 2) + Math.pow(points[2][1] - points[0][1], 2));
    
                double[] midPoint;
                double[] otherPoint;
                if(p0p1 < p1p2 && p0p1 < p2p0) {
                    midPoint = new double[]{(points[0][0] + points[1][0])/2, (points[0][1] + points[1][1])/2};
                    otherPoint = points[2];
                } else if(p1p2 < p0p1 && p1p2 < p2p0) {
                    midPoint = new double[]{(points[1][0] + points[2][0])/2, (points[1][1] + points[2][1])/2};
                    otherPoint = points[0];
                } else {
                    midPoint = new double[]{(points[2][0] + points[0][0])/2, (points[2][1] + points[0][1])/2};
                    otherPoint = points[1];
                }
    
                double theta = Math.toDegrees(Math.atan((otherPoint[1]-midPoint[1])/(otherPoint[0]-midPoint[0])));
                if(otherPoint[0] >= midPoint[0]) theta = -90 - theta;
                else theta = 90 - theta;
                System.out.println("Angle: " + theta);
            }
            
        } else {
            // System.out.println("no new frame");
            pipeline.drawBoundingBox(mat);
            source.putFrame(mat);
        }
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interupted) {
        System.out.println("Finished");
    }
}
