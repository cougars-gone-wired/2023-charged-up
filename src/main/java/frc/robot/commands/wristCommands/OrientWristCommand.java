package frc.robot.commands.wristCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.WristSubsystem;

public class OrientWristCommand extends SequentialCommandGroup {
    WristSubsystem m_wristSubsystem;

    public OrientWristCommand(WristSubsystem wristSubsystem) {
        m_wristSubsystem = wristSubsystem;

        addCommands(new ZeroWristCommand(m_wristSubsystem), new TrackConeCommand(m_wristSubsystem));
    }
}
