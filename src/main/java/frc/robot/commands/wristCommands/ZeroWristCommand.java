package frc.robot.commands.wristCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.WristSubsystem;

public class ZeroWristCommand extends CommandBase {
    private WristSubsystem m_wristSubsystem;

    public ZeroWristCommand(WristSubsystem wristSubsystem) {
        m_wristSubsystem = wristSubsystem;

        addRequirements(m_wristSubsystem);
    }

    @Override
    public void initialize() {
        m_wristSubsystem.stopMoving();
    }

    @Override
    public void execute() {
        m_wristSubsystem.rotateToAngle(0, false);
    }

    @Override
    public boolean isFinished() {
        return m_wristSubsystem.getAligned();
    }

    @Override
    public void end(boolean interrupted) {
        m_wristSubsystem.stopMoving();
        if(!interrupted) m_wristSubsystem.reset();
    }
}
