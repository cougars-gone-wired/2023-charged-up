package frc.robot.commands.wristCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.WristSubsystem;

public class RotateCCWCommand extends CommandBase {
    private WristSubsystem m_wristSubsystem;

    public RotateCCWCommand(WristSubsystem wristSubsystem) {
        m_wristSubsystem = wristSubsystem;

        addRequirements(m_wristSubsystem);
    }

    @Override
    public void initialize() {
        m_wristSubsystem.stopMoving();
    }

    @Override
    public void execute() {
        m_wristSubsystem.rotateToRelativeAngle(2);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_wristSubsystem.stopMoving();
    }
}
