package frc.robot.commands.endEffectorCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.EndEffectorSubsystem;


public class RollOutCommand extends CommandBase {
    private EndEffectorSubsystem m_endEffectorSubsystem;

    public RollOutCommand(EndEffectorSubsystem endEffectorSubsystem) {
        m_endEffectorSubsystem = endEffectorSubsystem;

        addRequirements(m_endEffectorSubsystem);
    }
    
    @Override
    public void initialize() {
        m_endEffectorSubsystem.stopEndRollers();
    }

    @Override
    public void execute() {
        m_endEffectorSubsystem.EndRollersOut();
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_endEffectorSubsystem.stopEndRollers();
    }
}
