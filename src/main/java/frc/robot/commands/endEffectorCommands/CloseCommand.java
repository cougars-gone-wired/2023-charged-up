package frc.robot.commands.endEffectorCommands;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.EndEffectorSubsystem;

public class CloseCommand extends CommandBase{
    private final EndEffectorSubsystem m_EndEffectorSubsystem;

    public CloseCommand(EndEffectorSubsystem EndEffectorSubsystem) {
        m_EndEffectorSubsystem = EndEffectorSubsystem;

        addRequirements(EndEffectorSubsystem);
    } 
    
    @Override
    public void initialize() {
    
    }

    @Override
    public void execute() {
        m_EndEffectorSubsystem.solenoidsRetract();
        // m_EndEffectorSubsystem.stopEndRollers();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
