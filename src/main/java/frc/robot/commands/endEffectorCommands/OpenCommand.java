package frc.robot.commands.endEffectorCommands;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.EndEffectorSubsystem;

public class OpenCommand extends CommandBase{
    private EndEffectorSubsystem m_EndEffectorSubsystem;

    public OpenCommand(EndEffectorSubsystem endEffectorSubsystem) {
        m_EndEffectorSubsystem = endEffectorSubsystem;

        addRequirements(endEffectorSubsystem);
    }
 
    @Override
    public void initialize() {

    }

    @Override
    public void execute() {
        m_EndEffectorSubsystem.solenoidsExtend();
        // m_EndEffectorSubsystem.EndRollersIn();
    }


    @Override
    public boolean isFinished() {
        return true;
    }
}
