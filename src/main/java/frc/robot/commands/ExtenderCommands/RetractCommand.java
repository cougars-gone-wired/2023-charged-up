package frc.robot.commands.ExtenderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ExtensionSubsystem;

public class RetractCommand extends CommandBase {
    private ExtensionSubsystem m_extensionSubsystem;

    public RetractCommand(ExtensionSubsystem extensionSubsystem) {
        m_extensionSubsystem = extensionSubsystem;

        addRequirements(m_extensionSubsystem);
    }

    @Override
    public void execute() {
        m_extensionSubsystem.setWinchMotorSpeed(-1);
        // m_extensionSubsystem.setWinchRetracting();
    }

    @Override
    public boolean isFinished() {
        return m_extensionSubsystem.getRevLimitClosed();
    }

    @Override
    public void end(boolean interrupted) {
        m_extensionSubsystem.stopWinchMoving();

        if(!interrupted) {
            m_extensionSubsystem.zeroEncoderPosition();
        }
    }

}
