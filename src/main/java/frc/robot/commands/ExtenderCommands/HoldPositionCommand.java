package frc.robot.commands.ExtenderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ExtensionSubsystem;

public class HoldPositionCommand extends CommandBase {
    private ExtensionSubsystem m_extensionSubsystem;
    private int kHoldPosition;

    public HoldPositionCommand(ExtensionSubsystem extensionSubsystem) {
        m_extensionSubsystem = extensionSubsystem;

        addRequirements(m_extensionSubsystem);
    }

    @Override
    public void initialize() {
        kHoldPosition = m_extensionSubsystem.getEncoderPosition();
    }

    @Override
    public void execute() {
        m_extensionSubsystem.goToPosition(kHoldPosition);
    }

    @Override
    public void end(boolean interrupted) {
        m_extensionSubsystem.stopWinchMoving();
    }
}
