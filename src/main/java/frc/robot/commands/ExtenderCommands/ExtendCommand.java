package frc.robot.commands.ExtenderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ExtensionSubsystem;

public class ExtendCommand extends CommandBase {
    private ExtensionSubsystem m_extensionSubsystem;

    public ExtendCommand(ExtensionSubsystem extensionSubsystem) {
        m_extensionSubsystem = extensionSubsystem;

        addRequirements(m_extensionSubsystem);
    }

    @Override
    public void execute() {
        m_extensionSubsystem.setWinchExtending();
    }

    @Override
    public void end(boolean interrupted) {
        m_extensionSubsystem.stopWinchMoving();
    }

}
