package frc.robot.commands.ExtenderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ExtensionSubsystem;

public class MoveToPositionCommand extends CommandBase {
    private ExtensionSubsystem m_extensionSubsystem;
    private int kSetpoint;
    private double error;

    private final double threshold = 500;
    
    public MoveToPositionCommand(ExtensionSubsystem extensionSubsystem, int setpoint) {
        m_extensionSubsystem = extensionSubsystem;
        kSetpoint = setpoint;

        addRequirements(m_extensionSubsystem);
    }
    
    @Override
    public void execute() {
        error = m_extensionSubsystem.getEncoderPosition() - kSetpoint;

        // if(error > 0) {
        //     m_extensionSubsystem.setWinchExtending();
        // } else if(error < 0) {
        //     m_extensionSubsystem.setWinchRetracting();
        // }

        m_extensionSubsystem.goToPosition(kSetpoint);
    }

    @Override
    public boolean isFinished() {
        return Math.abs(error) < threshold;
    }

    @Override
    public void end(boolean interrupted) {
        if(interrupted) m_extensionSubsystem.stopWinchMoving();
    }

}
