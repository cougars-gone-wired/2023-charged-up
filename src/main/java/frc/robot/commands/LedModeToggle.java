package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Limelight;

public class LedModeToggle extends CommandBase{
    private final Limelight m_mode;

    public LedModeToggle(Limelight mode) {
        m_mode = mode;
    }

    @Override
    public void initialize() {}

    @Override
    public void execute() {
        m_mode.ledModeToggle();
    }

    @Override
    public void end(boolean interrupted) {}

    @Override
    public boolean isFinished() {
        return true;
    }
}
