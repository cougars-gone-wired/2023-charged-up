package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Limelight;

public class CamModeToggle extends CommandBase{
    private final Limelight m_visionToggle;

    public CamModeToggle(Limelight visionToggle) {
        m_visionToggle = visionToggle;
    }

    @Override
    public void initialize() {}

    @Override
    public void execute() {
        m_visionToggle.camModeToggle();
    }

    @Override
    public void end(boolean interrupted) {}

    @Override
    public boolean isFinished() {
        return true;
    }
}