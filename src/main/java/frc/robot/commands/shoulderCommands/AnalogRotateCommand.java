package frc.robot.commands.shoulderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ShoulderSubsystem;

import java.util.function.DoubleSupplier;

public class AnalogRotateCommand extends CommandBase {
    private ShoulderSubsystem m_shoulderSubsystem;
    private DoubleSupplier m_speedSupplier;

    public AnalogRotateCommand(ShoulderSubsystem shoulderSubsystem, DoubleSupplier speedSupplier) {
        m_shoulderSubsystem = shoulderSubsystem;
        m_speedSupplier = speedSupplier;

        addRequirements(m_shoulderSubsystem);
    }

    @Override
    public void execute() {
        m_shoulderSubsystem.setRotation(m_speedSupplier.getAsDouble());
    }

    @Override
    public void end(boolean interrupted) {
        m_shoulderSubsystem.stopShoulderMotors();
    }
}
