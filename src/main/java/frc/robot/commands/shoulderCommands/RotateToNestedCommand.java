package frc.robot.commands.shoulderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ShoulderSubsystem;

public class RotateToNestedCommand extends CommandBase {
    private ShoulderSubsystem m_shoulderSubsystem;
    
    public RotateToNestedCommand(ShoulderSubsystem shoulderSubsystem) {
        m_shoulderSubsystem = shoulderSubsystem;

        addRequirements(m_shoulderSubsystem);
    }

    @Override
    public void execute() {
        m_shoulderSubsystem.shoulderDown();
    }

    @Override
    public boolean isFinished() {
        return m_shoulderSubsystem.atNestedRotation();
    }

    @Override
    public void end(boolean interrupted) {
        m_shoulderSubsystem.stopShoulderMotors();
    }
}
