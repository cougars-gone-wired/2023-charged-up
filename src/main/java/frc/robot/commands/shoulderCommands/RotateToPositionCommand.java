package frc.robot.commands.shoulderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ShoulderSubsystem;

public class RotateToPositionCommand extends CommandBase {
    private ShoulderSubsystem m_shoulderSubsystem;
    private double kAngle;

    public RotateToPositionCommand(ShoulderSubsystem shoulderSubsystem, double angle) {
        m_shoulderSubsystem = shoulderSubsystem;
        kAngle = angle;

        addRequirements(m_shoulderSubsystem);
    }

    @Override
    public void execute() {
        if(m_shoulderSubsystem.getShoulderAngle() - kAngle > 0) {
            m_shoulderSubsystem.shoulderUp();
        } else {
            m_shoulderSubsystem.shoulderDown();
        }
    }

    @Override
    public boolean isFinished() {
        return m_shoulderSubsystem.atAngle(kAngle, 0.01);
    }

    @Override
    public void end(boolean interrupted) {
        m_shoulderSubsystem.stopShoulderMotors();
    }
}
