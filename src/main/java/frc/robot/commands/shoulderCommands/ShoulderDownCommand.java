package frc.robot.commands.shoulderCommands;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ShoulderSubsystem;

public class ShoulderDownCommand extends CommandBase{
    private final ShoulderSubsystem m_shoulderSubsystem;
    
    public ShoulderDownCommand(ShoulderSubsystem shoulderSubsystem) {
        m_shoulderSubsystem = shoulderSubsystem;

        addRequirements(shoulderSubsystem);
    }

    @Override
    public void initialize() {}

    @Override
    public void execute() {
        m_shoulderSubsystem.slowShoulderDown();
    }

    @Override
    public void end(boolean interrupted) {
        m_shoulderSubsystem.stopShoulderMotors();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
