package frc.robot.commands.shoulderCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ShoulderSubsystem;

public class RotateToConePlaceCommand extends CommandBase {
    private ShoulderSubsystem m_shoulderSubsystem;

    public RotateToConePlaceCommand(ShoulderSubsystem shoulderSubsystem) {
        m_shoulderSubsystem = shoulderSubsystem;

        addRequirements(m_shoulderSubsystem);
    }

    @Override
    public void execute() {
        if(m_shoulderSubsystem.getShoulderAngle() - Constants.ShoulderConstants.CONE_NODE_ENCODER_ANGLE > 0) {
            m_shoulderSubsystem.shoulderUp();
        } else {
            m_shoulderSubsystem.shoulderDown();
        }
    }

    @Override
    public boolean isFinished() {
        return m_shoulderSubsystem.atAngle(Constants.ShoulderConstants.CONE_NODE_ENCODER_ANGLE, 0.005);
    }

    @Override
    public void end(boolean interrupted) {
        m_shoulderSubsystem.stopShoulderMotors();
    }
}
