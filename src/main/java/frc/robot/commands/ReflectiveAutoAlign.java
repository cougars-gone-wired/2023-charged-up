package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Limelight;
import frc.robot.subsystems.DrivetrainSubsystem;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.controller.PIDController;

public class ReflectiveAutoAlign extends CommandBase{
    public final DrivetrainSubsystem m_drivetrain;
    public final Limelight m_limelight;
    private ChassisSpeeds chassisSpeed;

    private PIDController strafeController;
    private final double strafeKp = 0.07;
    private final double strafeKi = 0;
    private final double strafeKd = 0;
    private double strafeSetpoint = 0;

    public ReflectiveAutoAlign(DrivetrainSubsystem driveTrain, Limelight limelight, double setPoint) {
        m_drivetrain = driveTrain;
        m_limelight = limelight;
        strafeSetpoint = setPoint;

        strafeController = new PIDController(strafeKp, strafeKi, strafeKd);
        strafeController.setSetpoint(strafeSetpoint);

        chassisSpeed = new ChassisSpeeds(0,0,0);

        addRequirements(m_drivetrain, m_limelight);
    }

    @Override
    public void initialize() {
        System.out.println("reflective init");
    }

    @Override
    public void execute() {
        chassisSpeed = new ChassisSpeeds(0, strafeController.calculate(m_limelight.getTX()), 0);
        m_drivetrain.drive(chassisSpeed);
        System.out.println(strafeController.calculate(m_limelight.getTX()));
    }

    @Override
    public boolean isFinished() {
        if(Math.abs(strafeController.calculate(m_limelight.getTX())) <= 0.3) {
            m_limelight.setPipelineFiducial();
            System.out.println("reflective end");
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void end(boolean interrupted) {
        chassisSpeed = new ChassisSpeeds(0,0, 0);
        m_drivetrain.drive(chassisSpeed);
    }
}
