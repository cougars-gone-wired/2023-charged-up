package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DrivetrainSubsystem;


public class CalibrateCommand extends CommandBase {
    private DrivetrainSubsystem m_drivetrainSubsystem;

    public CalibrateCommand(DrivetrainSubsystem drivetrainSubsystem) {
        m_drivetrainSubsystem = drivetrainSubsystem;
        
        addRequirements(m_drivetrainSubsystem);
    }

    @Override
    public void initialize() {
        m_drivetrainSubsystem.recal();        
    }

    @Override
    public boolean isFinished() {
        return !m_drivetrainSubsystem.calibrating();
    }

    @Override
    public void end(boolean interrupted) {
        m_drivetrainSubsystem.setOffsets();
    }
}
