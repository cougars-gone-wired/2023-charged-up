package frc.robot.commands.driveCommands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DrivetrainSubsystem;

public class ZeroCommand extends CommandBase{
    private final DrivetrainSubsystem m_driveTrain;

    public ZeroCommand(DrivetrainSubsystem driveTrain) {
        m_driveTrain = driveTrain;
    }

    @Override
    public void execute() {
        m_driveTrain.zeroGyroscope();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
