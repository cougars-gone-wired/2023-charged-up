package frc.robot.commands;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Limelight;
import frc.robot.subsystems.DrivetrainSubsystem;
import edu.wpi.first.math.controller.PIDController;

public class AutoAlign extends CommandBase{
    public final Limelight m_limelight;
    public final DrivetrainSubsystem m_drivetrain;
    private ChassisSpeeds chassisSpeed;

    private PIDController distanceController;
    private PIDController rotationController;
    private PIDController strafeController;

    private final double distanceKp = 0.5;
    private final double distanceKi = 0.07;
    private final double distanceKd = 0;
    private final double strafeKp = 1.6;
    private final double strafeKi = 0.1;
    private final double strafeKd = 0;
    private final double rotationKp = Math.PI/48;
    private final double rotationKi = 0;
    private final double rotationKd = 0;

    private final double distanceSetpoint = -0.7; //-0.8;
    private final double strafeSetpoint = -0.1;
    private final double rotationSetpoint = 0.6;

    private double lastVY;
    private double vy;

    public AutoAlign(DrivetrainSubsystem drivetrain, Limelight limelight) {
        m_drivetrain = drivetrain;
        m_limelight = limelight;

        distanceController = new PIDController(distanceKp, distanceKi, distanceKd);
        distanceController.setSetpoint(distanceSetpoint);
        strafeController = new PIDController(strafeKp, strafeKi, strafeKd);
        strafeController.setSetpoint(strafeSetpoint);
        rotationController = new PIDController(rotationKp, rotationKi, rotationKd);
        rotationController.setSetpoint(rotationSetpoint);

        chassisSpeed = new ChassisSpeeds(0,0,0);

        addRequirements(m_drivetrain, m_limelight);
    }
    
    @Override
    public void initialize() {
        m_limelight.setPipelineFiducial();
        System.out.println("init");
        lastVY = 0;
    }

    @Override
    public void execute() {

        if(m_limelight.getTV() > 0) {
            double vx = distanceController.calculate(m_limelight.getTZ());
            if(Math.abs(vx) <= 0.07) vx *= 0;
            vy = strafeController.calculate(m_limelight.getJTX());
            lastVY = (vy != 0) ? vy : lastVY;
            if(Math.abs(vy) <= 0.04) vy *= 0;
            double vr = rotationController.calculate(-m_limelight.getRY());
            if(Math.abs(vr) <= 0.05) vr *= 0;
            
            chassisSpeed = new ChassisSpeeds(vx, -vy, vr);
            m_drivetrain.drive(chassisSpeed);
        } else {
            double rot = Math.PI/4;

            if(lastVY > 0) {
                chassisSpeed = new ChassisSpeeds(0,0,-rot);
                System.out.println("positive");
            } else if(lastVY < 0) {
                chassisSpeed = new ChassisSpeeds(0,0,rot);
                System.out.println("negative");
            } else {
                chassisSpeed = new ChassisSpeeds(0, 0, 0);
                System.out.println("zero");
            }

            m_drivetrain.drive(chassisSpeed);
        }

        SmartDashboard.getNumberArray("Distance", new double[]{m_limelight.getTZ()});

    }

    @Override
    public boolean isFinished() {
        if(Math.abs(distanceController.calculate(m_limelight.getTZ())) <= 0.2 && Math.abs(strafeController.calculate(m_limelight.getJTX())) <= 0.11 && Math.abs(rotationController.calculate(-m_limelight.getRY())) <= 0.1 && Math.abs(lastVY) <= 0.4) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void end(boolean interrupted) {
        m_drivetrain.drive(new ChassisSpeeds(0,0,0)); 
        lastVY = 0;
    }
}
