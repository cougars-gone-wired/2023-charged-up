package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.DutyCycleEncoder;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import static frc.robot.Constants.ShoulderConstants.*;

import edu.wpi.first.math.MathUtil;

public class ShoulderSubsystem extends SubsystemBase {
    private WPI_TalonFX m_shoulderMotor;
    private DutyCycleEncoder encoder;
    private double encoderAngle;

    public ShoulderSubsystem() {
        m_shoulderMotor = new WPI_TalonFX(SHOULDER_MOTOR_ID);
        encoder = new DutyCycleEncoder(SHOULDER_ENCODER_ID);
        encoder.setDutyCycleRange(1/1025.0, 1024.0/1025.0);
        encoder.setPositionOffset(ENCODER_OFFSET);
    }

    public void shoulderUp() {
        // if(!atMaxRotation())
            m_shoulderMotor.set(SHOULDER_MOTOR_SPEED);
        // else 
        //     shoulderMotors.set(0);
    }
    
    public void shoulderDown() {
        // if(!atMinRotation())
            m_shoulderMotor.set(-SHOULDER_MOTOR_SPEED);
        // else 
        //     shoulderMotors.set(0);
    }

    public void slowShoulderUp() {
        m_shoulderMotor.set(SHOULDER_MOTOR_SPEED_SLOW);
    }

    public void slowShoulderDown() {
        m_shoulderMotor.set(-SHOULDER_MOTOR_SPEED_SLOW);
    }

    public void setRotation(double speed) {
        if(speed > 0 && !atPlacementRotation()) {
            m_shoulderMotor.set(Math.copySign(MathUtil.clamp(Math.abs(speed), MIN_SPEED, MAX_SPEED), speed));
        } else if(speed < 0 && !atNestedRotation()) {
            m_shoulderMotor.set(Math.copySign(MathUtil.clamp(Math.abs(speed), MIN_DOWN_SPEED, MAX_DOWN_SPEED), speed));
        } else {
            m_shoulderMotor.set(0);
        }
    }

    public double getShoulderAngle() {
        return encoderAngle;
    }

    public void stopShoulderMotors() {
        m_shoulderMotor.set(0);
    }


    
    
    // True if encoderAngle is at the MAX_ENCODER_ANGLE (aka the lowest position)
    public boolean atNestedRotation() {
        return encoderAngle >= MAX_ENCODER_ANGLE;
    }


    
    // True if encoderAngle is at the MIN_ENCODER_ANGLE (aka the highest position)
    public boolean atPlacementRotation() {
        return encoderAngle <= MIN_ENCODER_ANGLE;
    }

    
    public boolean atGroundIntakeRotation(double threshold) {
        return encoderAngle >= GROUND_INTAKE_ENCODER_ANGLE - threshold && encoderAngle <= GROUND_INTAKE_ENCODER_ANGLE + threshold;
    }

    public boolean atSlideIntakeRotation(double threshold) {
        return encoderAngle >= SLIDE_INTAKE_ENCODER_ANGLE - threshold && encoderAngle <= SLIDE_INTAKE_ENCODER_ANGLE + threshold;
    }

    public boolean atAngle(double angle, double threshold) {
        return encoderAngle >= angle - threshold && encoderAngle <= angle + threshold;
    }

    @Override
    public void periodic() {
        encoderAngle = encoder.getAbsolutePosition() - encoder.getPositionOffset();
        if(encoderAngle < 0) encoderAngle += 1;
        SmartDashboard.putNumber("Shoulder Encoder", encoderAngle);
    }
}
