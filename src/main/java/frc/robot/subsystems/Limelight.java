package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

import java.lang.reflect.Array;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;

public class Limelight extends SubsystemBase{

    private NetworkTable table = NetworkTableInstance.getDefault().getTable("limelight");
    private NetworkTable fmsTable = NetworkTableInstance.getDefault().getTable("FMSInfo");

    private NetworkTableEntry tv;
    private NetworkTableEntry tx;
    private NetworkTableEntry ty;
    private NetworkTableEntry ta;

    private NetworkTableEntry camMode;
    private NetworkTableEntry ledMode;
    private NetworkTableEntry camTran;
    private NetworkTableEntry botPose;
    private NetworkTableEntry targetPose;

    private NetworkTableEntry alliance;
    private NetworkTableEntry tid;

    private NetworkTableEntry pipeline;

    public Limelight() {
        tv = table.getEntry("tv");
        tx = table.getEntry("tx");
        ty = table.getEntry("ty");
        ta = table.getEntry("ta");

        camMode = table.getEntry("camMode");
        ledMode = table.getEntry("ledMode");

        camTran = table.getEntry("camerapose_targetspace");
        botPose = table.getEntry("botpose_targetspace");
        // targetPose = table.getEntry("targetpose_robotspace");
        alliance = fmsTable.getEntry("IsRedAlliance");
        tid = table.getEntry("tid");

        pipeline = table.getEntry("pipeline");

        initialize();
    }

    public void initialize() {
        camMode.setNumber(0);
        ledMode.setNumber(1);
    }

    public void camModeToggle() {
        if (camMode.getDouble(0) == 0) {
            camMode.setNumber(1);
        } else {
            camMode.setNumber(0);
        }
    }

    public void ledModeToggle() {
        if (ledMode.getDouble(1) == 1) {
            ledMode.setNumber(3);
        } else {
            ledMode.setNumber(1);
        }
    }

    public String getAliance() {
        
        if(alliance.getBoolean(false) == true) {
            return "red";
        } else {
            return "blue";
        }
    }

    public double getRZ() {
        try {
            return camTran.getDoubleArray(new double[0])[5];
        } catch (Exception e) {
            return 0;
        }
    }

    public double getRY() {
        try {
            return camTran.getDoubleArray(new double[0])[4];
        } catch (Exception e) {
            return 0;
        }
    }

    public double getRX() {
        try {
            return camTran.getDoubleArray(new double[0])[3];
        } catch (Exception e) {
            return 0;
        }
    }

    public double getJTX() {
        try {
            return botPose.getDoubleArray(new double[0])[0];
        } catch (Exception e) {
            return 0;
        }
    }

    public int getTV() {
        if(getAliance() == "red") {
            if(tid.getDouble(0) == 1.0 || tid.getDouble(0) == 2.0 || tid.getDouble(0) == 3.0 || tid.getDouble(0) == 5.0) {
                return 1;
            } else {
                return 0;
            }
        } else {
            if(tid.getDouble(0) == 6.0 || tid.getDouble(0) == 7.0 || tid.getDouble(0) == 8.0 || tid.getDouble(0) == 4.0) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public double getTZ() {
        try {
            return botPose.getDoubleArray(new double[0])[2];
        } catch (Exception e) {
            return 0;
        }
    }

    public double getTX() {
        return tx.getDouble(0);
    }

    public void setPipelineFiducial() {
        ledMode.setNumber(1);
        pipeline.setNumber(0);
    }

    public void setPipelineReflective() {
        ledMode.setNumber(3);
        pipeline.setNumber(1);
    }

    @Override
    public void periodic() {
        SmartDashboard.putNumber("tx: ", tx.getDouble(0));
        SmartDashboard.putNumber("ty: ", ty.getDouble(0));
        SmartDashboard.putNumber("tz", getTZ());
        SmartDashboard.putNumber("tv: ", tv.getDouble(0));
        SmartDashboard.putNumber("ta: ", ta.getDouble(0));
        SmartDashboard.putNumber("RY: ", getRY());
        SmartDashboard.putNumber("JTX: ", getJTX());
        SmartDashboard.putNumber("TZ: ", getTZ());
        SmartDashboard.putNumberArray("Botpose: ", botPose.getDoubleArray(new double[0]));
    }
}
