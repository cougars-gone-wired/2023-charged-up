package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DutyCycleEncoder;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import static frc.robot.Constants.WristConstants.*;

public class WristSubsystem extends SubsystemBase {
    private DutyCycleEncoder m_absEncoder;
    private WPI_TalonSRX m_wristMotor;
    private boolean aligned = false;
    private int angleThresh = 1;
    private double relativeOffset;

    public WristSubsystem() {
        m_absEncoder = new DutyCycleEncoder(ENCODER_ID);
        m_absEncoder.setDutyCycleRange(1/1025.0, 1024/1025.0);
        m_absEncoder.setPositionOffset(ENCODER_OFFSET);
        m_wristMotor = new WPI_TalonSRX(WRIST_MOTOR_ID);
        m_wristMotor.setNeutralMode(NeutralMode.Brake);
        relativeOffset = getAbsolutePosition();
    }


    public double getAbsoluteAngle() {
        double ang = getAbsolutePosition() * 360;
        if(ang > 180) ang -= 360;
        return -ang;
    }

    public double getRelativeAngle() {
        double ang = getPosition() * 360;
        return -ang;
    }

    public void moveForward() {
        m_wristMotor.set(0.3);
    }

    public void stopMoving() {
        m_wristMotor.set(0);
    }

    public void reset() {
        m_absEncoder.reset();
    }


    // Be sure to make an implementation that corrects for overrotation using the relative angle

    // Takes the angle of the cone from the pipeline and rotates the wrist to it
    public void rotateToRelativeAngle(double coneAngle) {
        rotateToAngle(getAbsoluteAngle() + coneAngle);
    }

    public void rotateToAngle(double angle) {
        rotateToAngle(angle, true);
    }

    // Takes an angle [-180, 180] and rotates to said angle
    public void rotateToAngle(double angle, boolean preventOverrot) {
        double currentAngle = getAbsoluteAngle();
        double relPos = getPosition(); // Minus of death
        double error;
        
        if(preventOverrot) {
            if(Math.abs(relPos) > 0.5) {
                currentAngle += Math.copySign(360, relPos);
            }
        }
        
        error = angle - currentAngle;
        if(Math.abs(angle) > 180) {
            error -= Math.copySign(360, error);
        }
        
        // aligned = Math.abs(error) < angleThresh;

        // PID Controller
        // if(Math.abs(error) > angleThresh) {
        //     error = Math.copySign(Math.max(0.1, Math.min(Math.abs(error) * 0.1, 0.7)), error);
        //     m_wristMotor.set(error);
        // }

        m_wristMotor.set((Math.abs(error) > angleThresh) ? Math.copySign(WRIST_SPEED, error) : 0);

        SmartDashboard.putNumber("Error", error);
    }

    public double getAbsolutePosition() {
        double ang = m_absEncoder.getAbsolutePosition() - m_absEncoder.getPositionOffset();
        if(ang < 0) ang += 1;
        return ang;
    }

    public double getPosition() {
        return -m_absEncoder.get();
    }

    public boolean getAligned() {
        return aligned;
    }

    public void setNotAligned() {
        aligned = false;
    }

    

    @Override
    public void periodic() {
        SmartDashboard.putNumber("Wrist Encoder AbsPosition", m_absEncoder.getAbsolutePosition());
        SmartDashboard.putNumber("Wrist AbsPosition", getAbsolutePosition());
        SmartDashboard.putNumber("Wrist Encoder Position", getPosition());
        SmartDashboard.putNumber("Wrist Encoder AbsAngle", getAbsoluteAngle());
        SmartDashboard.putNumber("Wrist Encoder Angle", getRelativeAngle());
    }
}
