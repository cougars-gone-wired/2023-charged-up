package frc.robot.subsystems;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import static frc.robot.Constants.ExtensionConstants.*;


public class ExtensionSubsystem extends SubsystemBase {
    
    private int encoderPosition;
    private boolean revLimitPressed;
    private boolean hasZeroed = false;

    private WPI_TalonFX m_winchMotor;
    

    public ExtensionSubsystem() {
        m_winchMotor = new WPI_TalonFX(WINCH_MOTOR_ID);
        m_winchMotor.configFactoryDefault();
        m_winchMotor.setInverted(true);
        m_winchMotor.config_kP(0, 0.1);
        m_winchMotor.configAllowableClosedloopError(0, 100);
        setMotorsBrake();
    }

    public void setWinchExtending() {
        m_winchMotor.set(WINCH_MOTOR_SPEED);
    }

    public void setWinchRetracting() {
        m_winchMotor.set(-WINCH_MOTOR_SPEED);
    }

    public void stopWinchMoving() {
        m_winchMotor.set(0);
    }

    public void goToPosition(int position) {
        if(hasZeroed)
        m_winchMotor.set(ControlMode.Position, position);
    }

    public void setWinchMotorSpeed(double speed) {
        m_winchMotor.set(speed);
    }

    public void zeroEncoderPosition() {
        m_winchMotor.setSelectedSensorPosition(0);
        hasZeroed = true;
    }

    public int getEncoderPosition() {
        return encoderPosition;
    }

    public boolean getRevLimitClosed() {
        return revLimitPressed;
    }

    public void setMotorsBrake() {
        m_winchMotor.setNeutralMode(NeutralMode.Brake);
    }
    
    public void setMotorsCoast() {
        m_winchMotor.setNeutralMode(NeutralMode.Coast);
    }
    
    
    @Override
    public void periodic() {
        encoderPosition = (int) m_winchMotor.getSelectedSensorPosition();
        SmartDashboard.putNumber("Winch Encoder", encoderPosition);

        revLimitPressed = m_winchMotor.isRevLimitSwitchClosed() == 1;
        SmartDashboard.putBoolean("Rev Limit Closed", revLimitPressed);
    }
}
