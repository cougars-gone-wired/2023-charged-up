package frc.robot.subsystems;
import com.ctre.phoenix.motorcontrol.TalonSRXControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import frc.robot.Constants;
import frc.robot.Constants.endEffectorConstants;
// import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
// import edu.wpi.first.wpilibj.PneumaticsModuleType;
// import static edu.wpi.first.wpilibj.DoubleSolenoid.Value;
// import edu.wpi.first.wpilibj.DoubleSolenoid;

public class EndEffectorSubsystem extends SubsystemBase{
    private WPI_TalonSRX endRollers;
    // private DoubleSolenoid solenoid1;

    public EndEffectorSubsystem() {
        endRollers = new WPI_TalonSRX(Constants.endEffectorConstants.ENDEFFECTOR_MOTOR_ID);
        endRollers.setInverted(false);
        // solenoid1 = new DoubleSolenoid(20, PneumaticsModuleType.CTREPCM, Constants.endEffectorConstants.ENDEFFECTOR_SOLENOIED1_ID1, Constants.endEffectorConstants.ENDEFFECTOR_SOLENOIED1_ID2);
        
        initialize();
    }
    
    public void initialize() {
        endRollers.set(TalonSRXControlMode.PercentOutput, 0);
        // solenoid1.set(Value.kOff);
    }

    public void EndRollersIn() {
        endRollers.set(TalonSRXControlMode.PercentOutput, Constants.endEffectorConstants.ENDEFFECTOR_MOTOR_SPEED);
    }

    public void EndRollersOut() {
        endRollers.set(TalonSRXControlMode.PercentOutput, -Constants.endEffectorConstants.ENDEFFECTOR_MOTOR_SPEED);
    }
    
    public void solenoidsExtend() {
        // solenoid1.set(Value.kForward);  
    }

    public void solenoidsRetract() {
        // solenoid1.set(Value.kReverse);
    }

    public void stopEndRollers() {
        endRollers.set(TalonSRXControlMode.PercentOutput, 0);        
    }

    public void stopSolenoids() {
        // solenoid1.set(Value.kOff);
    }
}
