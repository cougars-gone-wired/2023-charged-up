// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BooleanSupplier;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.commands.autoCommands.BalanceScoreAuto;
import frc.robot.commands.autoCommands.DoNothingCommand;
import frc.robot.commands.autoCommands.CubeIntakePosCommand;
import frc.robot.commands.autoCommands.CubeSpitCommand;
import frc.robot.commands.autoCommands.SafeNestCommand;
import frc.robot.commands.autoCommands.TaxiAutoCommand;
import frc.robot.commands.autoCommands.TaxiScoreAuto;
import frc.robot.commands.autoCommands.WallsideAutoCommand;
import frc.robot.commands.autoCommands.PathPlannerCommands;
import frc.robot.commands.autoCommands.PlaceConeHighCommand;
import frc.robot.commands.driveCommands.DefaultDriveCommand;
import frc.robot.commands.driveCommands.ZeroCommand;
import frc.robot.commands.endEffectorCommands.CloseCommand;
import frc.robot.commands.endEffectorCommands.OpenCommand;
import frc.robot.commands.endEffectorCommands.RollInCommand;
import frc.robot.commands.endEffectorCommands.RollOutCommand;
import frc.robot.commands.endEffectorCommands.StopRollingCommand;
import frc.robot.commands.shoulderCommands.AnalogRotateCommand;
import frc.robot.commands.shoulderCommands.RotateToConePlaceCommand;
import frc.robot.commands.shoulderCommands.RotateToGroundIntakeCommand;
import frc.robot.commands.shoulderCommands.RotateToHighCubeCommand;
import frc.robot.commands.shoulderCommands.RotateToMidCubeCommand;
import frc.robot.commands.shoulderCommands.RotateToNestedCommand;
import frc.robot.commands.shoulderCommands.RotateToPlaceCommand;
import frc.robot.commands.shoulderCommands.RotateToPositionCommand;
import frc.robot.commands.shoulderCommands.RotateToSlideIntakeCommand;
import frc.robot.commands.shoulderCommands.ShoulderDownCommand;
import frc.robot.commands.shoulderCommands.ShoulderUpCommand;
import frc.robot.commands.BalanceCommand;
import frc.robot.commands.ExtenderCommands.ExtendCommand;
import frc.robot.commands.ExtenderCommands.HoldPositionCommand;
import frc.robot.commands.ExtenderCommands.MoveToPositionCommand;
import frc.robot.commands.ExtenderCommands.RetractCommand;
import frc.robot.subsystems.DrivetrainSubsystem;
import frc.robot.subsystems.EndEffectorSubsystem;
import frc.robot.subsystems.ExtensionSubsystem;
import frc.robot.subsystems.ShoulderSubsystem;
import frc.robot.commands.AutoAlign;
import frc.robot.commands.ReflectiveAutoAlignCommandGroup;
import frc.robot.utils.BooleanXboxController;

import frc.robot.subsystems.Limelight;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  private final DrivetrainSubsystem m_drivetrainSubsystem =
    Constants.Subsystems.DRIVE_TRAIN_SUBSYSTEM
    ? new DrivetrainSubsystem()
    : null;
  private final ShoulderSubsystem m_ShoulderSubsystem = 
    Constants.Subsystems.SHOULDER_SUBSYSTEM
    ? new ShoulderSubsystem()
    : null;
  private final ExtensionSubsystem m_extensionSubsystem = 
    Constants.Subsystems.EXTENSION_SUBSYSTEM
    ? new ExtensionSubsystem()
    : null;
  private final EndEffectorSubsystem m_endEffectorSubsystem = 
    Constants.Subsystems.END_EFFECTOR_SUBSYSTEM
    ? new EndEffectorSubsystem()
    : null;

  private final Limelight m_limelight = new Limelight(); 

  private final BooleanXboxController m_manipulatorController = new BooleanXboxController(1);
  private final BooleanXboxController m_mobilityController = new BooleanXboxController(0);

  private PathPlannerCommands m_pathPlannerCommands;
  private Map<String, Command> m_ppCommandsMap;

  private final DoNothingCommand doNothingCommand = new DoNothingCommand();
  private final SendableChooser<Command> autoChooser = new SendableChooser<>();
  private final TaxiAutoCommand taxiAutoCommand = 
    m_drivetrainSubsystem != null
    ? new TaxiAutoCommand(m_drivetrainSubsystem)
    : null;
  private final WallsideAutoCommand redWallsideAutoCommand = 
    m_drivetrainSubsystem != null 
    ? new WallsideAutoCommand(m_drivetrainSubsystem, "red")
    : null;
  private final WallsideAutoCommand blueWallsideAutoCommand = 
    m_drivetrainSubsystem != null
    ? new WallsideAutoCommand(m_drivetrainSubsystem, "blue")
    : null;
  
  private final BalanceScoreAuto redBalanceScoreAuto = 
    m_endEffectorSubsystem != null && m_ShoulderSubsystem != null && m_ShoulderSubsystem != null && m_drivetrainSubsystem != null
    ? new BalanceScoreAuto(m_endEffectorSubsystem, m_ShoulderSubsystem, m_extensionSubsystem, m_drivetrainSubsystem, "red")
    : null;

  private final BalanceScoreAuto blueBalanceScoreAuto = 
    m_endEffectorSubsystem != null && m_ShoulderSubsystem != null && m_ShoulderSubsystem != null && m_drivetrainSubsystem != null
    ? new BalanceScoreAuto(m_endEffectorSubsystem, m_ShoulderSubsystem, m_extensionSubsystem, m_drivetrainSubsystem, "blue")
    : null;

  private final TaxiScoreAuto taxiScoreAuto = 
    m_endEffectorSubsystem != null && m_ShoulderSubsystem != null && m_ShoulderSubsystem != null && m_drivetrainSubsystem != null
    ? new TaxiScoreAuto(m_endEffectorSubsystem, m_ShoulderSubsystem, m_extensionSubsystem, m_drivetrainSubsystem)
    : null;

  public static String getAlliance() {
    if(DriverStation.getAlliance() == DriverStation.Alliance.Red) {
      return "red";
    } else {
      return "blue";
    }
  }

  /**
   * The container for the robot. Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    // Set up the default command for the drivetrain.
    // The controls are for field-oriented driving:
    // Left stick Y axis -> forward and backwards movement
    // Left stick X axis -> left and right movement
    // Right stick X axis -> rotation
    if (m_drivetrainSubsystem != null) {
        m_drivetrainSubsystem.setDefaultCommand(new DefaultDriveCommand(
                m_drivetrainSubsystem,
                () -> -modifyAxis(m_mobilityController.getLeftY()) * DrivetrainSubsystem.MAX_VELOCITY_METERS_PER_SECOND,
                () -> -modifyAxis(m_mobilityController.getLeftX()) * DrivetrainSubsystem.MAX_VELOCITY_METERS_PER_SECOND,
                () -> -modifyAxis(m_mobilityController.getRightX()) * DrivetrainSubsystem.MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND
        ));
    }

    // m_wristSubsystem.setDefaultCommand(new DefaultWristCommand(m_wristSubsystem));

    if(m_ShoulderSubsystem != null) {
        m_ShoulderSubsystem.setDefaultCommand(new AnalogRotateCommand(m_ShoulderSubsystem, () -> -deadband(m_manipulatorController.getLeftY(), 0.3)));
    }

    if(m_extensionSubsystem != null) {    
        m_extensionSubsystem.setDefaultCommand(new HoldPositionCommand(m_extensionSubsystem));
    }

    // Configure the autoChooser
    configureAutoChooser();
    
    // CameraServer.startAutomaticCapture("Wrist Camera", 0).setFPS(30);
    
    
    // Configure the button bindings
    configureButtonBindings();
  }

  public void configureAutoChooser() {
    var events = new HashMap<String, Command>();
    events.put("score_cone", new PlaceConeHighCommand(m_ShoulderSubsystem, m_extensionSubsystem, m_endEffectorSubsystem));
    events.put("score_cube", new CubeSpitCommand(m_endEffectorSubsystem));
    events.put("auto_balance", new BalanceCommand(m_drivetrainSubsystem));
    events.put("limelight", new AutoAlign(m_drivetrainSubsystem, m_limelight));
    events.put("intake", new RollInCommand(m_endEffectorSubsystem).raceWith(new WaitCommand(3)));
    events.put("retract", new RetractCommand(m_extensionSubsystem));
    // events.put("position_intake", new CubeIntakePosCommand(m_extensionSubsystem, m_ShoulderSubsystem));
    events.put("position_intake", new MoveToPositionCommand(m_extensionSubsystem, Constants.ExtensionConstants.CUBE_INTAKE_POS).alongWith(new WaitCommand(1).andThen(new RotateToPositionCommand(m_ShoulderSubsystem, Constants.ShoulderConstants.CUBE_INTAKE_SUCC_ENCODER_ANGLE))));
    events.put("position_score_cone", new PrintCommand("Score cone arm position!"));
    events.put("position_score_cube", new RotateToHighCubeCommand(m_ShoulderSubsystem).alongWith(new MoveToPositionCommand(m_extensionSubsystem, Constants.ExtensionConstants.CONE_MID_NODE_POS)));
    events.put("position_nest", new SafeNestCommand(m_extensionSubsystem, m_ShoulderSubsystem));
    m_pathPlannerCommands = new PathPlannerCommands(m_drivetrainSubsystem, events);
    m_ppCommandsMap = m_pathPlannerCommands.createPathPlannerCommands();

    // Command Options
    autoChooser.addOption("Do Nothing", doNothingCommand);
    m_ppCommandsMap.forEach((name, command) -> {
        autoChooser.addOption("Path Planner: "+name, command);
    });
    // autoChooser.addOption("Taxi", taxiAutoCommand);
    // autoChooser.addOption("Red Wallside Auto", redWallsideAutoCommand);
    // autoChooser.addOption("Blue Wallside Auto", blueWallsideAutoCommand);
    // autoChooser.addOption("Red Balance Score Auto", redBalanceScoreAuto);
    // autoChooser.addOption("Blue Balance Score Auto", blueBalanceScoreAuto);
    // autoChooser.addOption("Taxi Score", taxiScoreAuto);

    autoChooser.setDefaultOption("Do Nothing", doNothingCommand);
    SmartDashboard.putData("Auto Programs", autoChooser);
  }
  
  public void initialize() {
    m_limelight.initialize();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    // Back button zeros the gyroscope
    if (m_drivetrainSubsystem != null) {
        new Trigger(m_mobilityController::getBackButton)
                .onTrue(new ZeroCommand(m_drivetrainSubsystem));
        
        new Trigger(m_mobilityController::getAButton)
                .toggleOnTrue(new BalanceCommand(m_drivetrainSubsystem));
        
        // if(getAlliance() == "blue") {
        //   new Trigger(m_mobilityController::getPOVLeftButton).toggleOnTrue(m_ppCommandsMap.get("Align Cone Left"));
        //   new Trigger(m_mobilityController::getPOVRightButton).toggleOnTrue(m_ppCommandsMap.get("Align Cone Right"));
        // } else {
        //   new Trigger(m_mobilityController::getPOVRightButton).toggleOnTrue(m_ppCommandsMap.get("Align Cone Left"));
        //   new Trigger(m_mobilityController::getPOVLeftButton).toggleOnTrue(m_ppCommandsMap.get("Align Cone Right"));
        // }
        
        new Trigger(m_mobilityController::getRightBumper).whileTrue(new DefaultDriveCommand(
                m_drivetrainSubsystem,
                () -> -modifyAxis(m_mobilityController.getLeftY()) * DrivetrainSubsystem.MAX_VELOCITY_METERS_PER_SECOND/2.0,
                () -> -modifyAxis(m_mobilityController.getLeftX()) * DrivetrainSubsystem.MAX_VELOCITY_METERS_PER_SECOND/2.0,
                () -> -modifyAxis(m_mobilityController.getRightX()) * DrivetrainSubsystem.MAX_ANGULAR_VELOCITY_RADIANS_PER_SECOND/2.0
        ));

        // new Trigger(m_controller::getBButton)
        //         .onTrue(new CalibrateCommand(m_drivetrainSubsystem));   
    } 
    m_manipulatorController.setTriggerThreshold(0.2);

    if(m_extensionSubsystem != null) {
        new Trigger(m_manipulatorController::getLeftTriggerPressed)
                .whileTrue(new ExtendCommand(m_extensionSubsystem));
        new Trigger(m_manipulatorController::getLeftBumper)
                .whileTrue(new RetractCommand(m_extensionSubsystem));
    }
    
    if(m_endEffectorSubsystem != null) {
        new Trigger(new BooleanSupplier() {
        @Override
        public boolean getAsBoolean() {
            return m_manipulatorController.getRightY() <= -0.2;
        }
        }).onTrue(new OpenCommand(m_endEffectorSubsystem));
        new Trigger(new BooleanSupplier() {
        @Override
        public boolean getAsBoolean() {
            return m_manipulatorController.getRightY() >= 0.2;
        }
        }).onTrue(new CloseCommand(m_endEffectorSubsystem));
        new Trigger(m_manipulatorController::getRightTriggerPressed).onTrue(new RollInCommand(m_endEffectorSubsystem)).onFalse(new StopRollingCommand(m_endEffectorSubsystem));
        new Trigger(m_manipulatorController::getRightBumper).onTrue(new RollOutCommand(m_endEffectorSubsystem)).onFalse(new StopRollingCommand(m_endEffectorSubsystem));
    }

    // new Trigger(m_manipulatorController::getPOVUpButton)
    //         .onTrue(new ZeroWristCommand(m_wristSubsystem));
    // new Trigger(m_manipulatorController::getPOVRightButton)
    //         .whileTrue(new RotateCWCommand(m_wristSubsystem));
    // new Trigger(m_manipulatorController::getPOVLeftButton)
    //         .whileTrue(new RotateCCWCommand(m_wristSubsystem));

    if(m_ShoulderSubsystem != null) {
        new Trigger(m_manipulatorController::getXButton)
                .toggleOnTrue(new RotateToConePlaceCommand(m_ShoulderSubsystem));
        new Trigger(m_manipulatorController::getAButton)
                .toggleOnTrue(new RotateToMidCubeCommand(m_ShoulderSubsystem));
        new Trigger(m_manipulatorController::getBButton)
                .toggleOnTrue(new RotateToHighCubeCommand(m_ShoulderSubsystem));
        new Trigger(m_manipulatorController::getYButton).toggleOnTrue(new SafeNestCommand(m_extensionSubsystem, m_ShoulderSubsystem));
    
        new Trigger(m_manipulatorController::getPOVDownButton)
                .toggleOnTrue(new MoveToPositionCommand(m_extensionSubsystem, Constants.ExtensionConstants.CONE_MID_NODE_POS));
        new Trigger(m_manipulatorController::getPOVUpButton)
                .toggleOnTrue(new MoveToPositionCommand(m_extensionSubsystem, Constants.ExtensionConstants.CONE_UPPER_NODE_POS));

        // new Trigger(m_manipulatorController::getPOVRightButton).onTrue(new RotateToConePlaceCommand(m_ShoulderSubsystem).andThen(new MoveToPositionCommand(m_extensionSubsystem, Constants.ExtensionConstants.CONE_UPPER_NODE_POS)));
        // new Trigger(m_manipulatorController::getPOVLeftButton).onTrue(new RotateToConePlaceCommand(m_ShoulderSubsystem).andThen(new MoveToPositionCommand(m_extensionSubsystem, Constants.ExtensionConstants.CONE_MID_NODE_POS)));
        new Trigger(m_manipulatorController::getPOVRightButton).toggleOnTrue(new MoveToPositionCommand(m_extensionSubsystem, Constants.ExtensionConstants.CONE_SLIDER_INTAKE_POS));
        new Trigger(m_manipulatorController::getPOVLeftButton).toggleOnTrue(new CubeIntakePosCommand(m_extensionSubsystem, m_ShoulderSubsystem));

        // Shoulder override commands for those pesky situations
        new Trigger(m_manipulatorController::getStartButton).whileTrue(new ShoulderDownCommand(m_ShoulderSubsystem));
        new Trigger(m_manipulatorController::getBackButton).whileTrue(new ShoulderUpCommand(m_ShoulderSubsystem));
    }

    // new Trigger(m_manipulatorController::getStartButton).whileTrue(new MoveToPositionCommand(m_extensionSubsystem, 130000));
    // new Trigger(m_mobilityController::getLeftBumper).onTrue(new InstantCommand(m_limelight::ledModeToggle));
    // new Trigger(m_mobilityController::getStartButton).onTrue(new InstantCommand(m_limelight::camModeToggle));
    // SmartDashboard.putData("LED TOGGLE", new InstantCommand(m_limelight::ledModeToggle));
    // SmartDashboard.putData("CAM MODE TOGGLE", new InstantCommand(m_limelight::camModeToggle));
    // new Trigger(m_mobilityController::getBButton).toggleOnTrue(new AutoAlign(m_drivetrainSubsystem, m_limelight));
    // new Trigger(m_mobilityController::getPOVLeftButton).onTrue(new ReflectiveAutoAlignCommandGroup(m_drivetrainSubsystem, m_limelight, 0.3, 0.1));
    // new Trigger(m_mobilityController::getPOVRightButton).onTrue(new ReflectiveAutoAlignCommandGroup(m_drivetrainSubsystem, m_limelight, -0.3, 0.1));
    // SmartDashboard.putData(new SafeNestCommand(m_extensionSubsystem, m_ShoulderSubsystem));
    // SmartDashboard.putData(new PlaceConeHighCommand(m_ShoulderSubsystem, m_extensionSubsystem, m_endEffectorSubsystem));
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    return autoChooser.getSelected();
  }

  private static double deadband(double value, double deadband) {
    if (Math.abs(value) > deadband) {
      if (value > 0.0) {
        return (value - deadband) / (1.0 - deadband);
      } else {
        return (value + deadband) / (1.0 - deadband);
      }
    } else {
      return 0.0;
    }
  }

    private static double modifyAxis(double value) {
        // Deadband
        value = deadband(value, 0.05);

        // Square the axis
        value = Math.copySign(value * value, value);

        return value;
    }
}
