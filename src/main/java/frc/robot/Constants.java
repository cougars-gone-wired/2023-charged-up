// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    private static final String robot = "Bernard";

    public static final double TIME_MULT = 1.09;

    public static final class Subsystems {
        public static final boolean DRIVE_TRAIN_SUBSYSTEM;
        public static final boolean SHOULDER_SUBSYSTEM;
        public static final boolean EXTENSION_SUBSYSTEM;
        public static final boolean END_EFFECTOR_SUBSYSTEM;
        static {
            if(robot.equals("Bernard")) {
                DRIVE_TRAIN_SUBSYSTEM = true;
                SHOULDER_SUBSYSTEM = true;
                EXTENSION_SUBSYSTEM = true;
                END_EFFECTOR_SUBSYSTEM = true;
            } else if(robot.equals("Felon")) {
                DRIVE_TRAIN_SUBSYSTEM = true;
                SHOULDER_SUBSYSTEM = false;
                EXTENSION_SUBSYSTEM = false;
                END_EFFECTOR_SUBSYSTEM = false;
            } else {
                DRIVE_TRAIN_SUBSYSTEM = false;
                SHOULDER_SUBSYSTEM = false;
                EXTENSION_SUBSYSTEM = false;
                END_EFFECTOR_SUBSYSTEM = false;
            }
        }
    }
    public static final class SwerveConstants {
        /**
         * The left-to-right distance between the drivetrain wheels
         *
         * Should be measured from center to center.
         */
        public static final double DRIVETRAIN_TRACKWIDTH_METERS = 0.619125; // 24 3/8"
        /**
         * The front-to-back distance between the drivetrain wheels.
         *
         * Should be measured from center to center.
         */
        public static final double DRIVETRAIN_WHEELBASE_METERS = 0.8636; // 34"
        
        public static final int FRONT_LEFT_MODULE_DRIVE_MOTOR;
        public static final int FRONT_LEFT_MODULE_STEER_MOTOR;
        public static final int FRONT_LEFT_MODULE_STEER_ENCODER;
        public static final double FRONT_LEFT_MODULE_STEER_OFFSET;
    
        public static final int FRONT_RIGHT_MODULE_DRIVE_MOTOR;
        public static final int FRONT_RIGHT_MODULE_STEER_MOTOR;
        public static final int FRONT_RIGHT_MODULE_STEER_ENCODER;
        public static final double FRONT_RIGHT_MODULE_STEER_OFFSET;
    
        public static final int BACK_LEFT_MODULE_DRIVE_MOTOR;
        public static final int BACK_LEFT_MODULE_STEER_MOTOR;
        public static final int BACK_LEFT_MODULE_STEER_ENCODER;
        public static final double BACK_LEFT_MODULE_STEER_OFFSET;
    
        public static final int BACK_RIGHT_MODULE_DRIVE_MOTOR;
        public static final int BACK_RIGHT_MODULE_STEER_MOTOR;
        public static final int BACK_RIGHT_MODULE_STEER_ENCODER;
        public static final double BACK_RIGHT_MODULE_STEER_OFFSET;

        static {
            if(robot.equals("Bernard")) {
                
                // John
                BACK_RIGHT_MODULE_DRIVE_MOTOR = 5;
                BACK_RIGHT_MODULE_STEER_MOTOR = 3;
                BACK_RIGHT_MODULE_STEER_ENCODER = 11;
                BACK_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(184.306640625-180);
                
                // Mister
                BACK_LEFT_MODULE_DRIVE_MOTOR = 7;
                BACK_LEFT_MODULE_STEER_MOTOR = 2;
                BACK_LEFT_MODULE_STEER_ENCODER = 9;
                BACK_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(229.21874999999997-180);
                
                // Patrick
                FRONT_RIGHT_MODULE_DRIVE_MOTOR = 4;
                FRONT_RIGHT_MODULE_STEER_MOTOR = 6;
                FRONT_RIGHT_MODULE_STEER_ENCODER = 10;
                FRONT_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(339.2578125-180);
                
                // Bernard
                FRONT_LEFT_MODULE_DRIVE_MOTOR = 1;
                FRONT_LEFT_MODULE_STEER_MOTOR = 8;
                FRONT_LEFT_MODULE_STEER_ENCODER = 12;
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(68.37890625+180);
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(102.56367187500001 + 180);
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(131.8359375+180);
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(29.003906250000004+180);
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(336.005859375-180);
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(10.37109375+180);
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(85.25390625+180);
                // FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(122.6941+180);
                FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(201.796875-180);
                
                
            } else if(robot.equals("Felon")) {
                // since these were put on backwards, rotated IDs and changed offsets by 180
                FRONT_LEFT_MODULE_DRIVE_MOTOR = 9;
                FRONT_LEFT_MODULE_STEER_MOTOR = 4;
                FRONT_LEFT_MODULE_STEER_ENCODER = 10;
                FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(65.478515625 + 180);

                FRONT_RIGHT_MODULE_DRIVE_MOTOR = 1;
                FRONT_RIGHT_MODULE_STEER_MOTOR = 3;
                FRONT_RIGHT_MODULE_STEER_ENCODER = 12;
                FRONT_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(340.83984375 - 180);

                BACK_LEFT_MODULE_DRIVE_MOTOR = 5;
                BACK_LEFT_MODULE_STEER_MOTOR = 6;
                BACK_LEFT_MODULE_STEER_ENCODER = 13;
                BACK_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(64.07226525 + 180);

                BACK_RIGHT_MODULE_DRIVE_MOTOR = 8;
                BACK_RIGHT_MODULE_STEER_MOTOR = 7;
                BACK_RIGHT_MODULE_STEER_ENCODER = 11;
                BACK_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(168.4863984 + 180);
            } else {
                // LEAVE THESE ALL AS ZEROS

                FRONT_LEFT_MODULE_DRIVE_MOTOR = 0;
                FRONT_LEFT_MODULE_STEER_MOTOR = 0;
                FRONT_LEFT_MODULE_STEER_ENCODER = 0;
                FRONT_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(0);

                FRONT_RIGHT_MODULE_DRIVE_MOTOR = 0;
                FRONT_RIGHT_MODULE_STEER_MOTOR = 0;
                FRONT_RIGHT_MODULE_STEER_ENCODER = 0;
                FRONT_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(0);

                BACK_LEFT_MODULE_DRIVE_MOTOR = 0;
                BACK_LEFT_MODULE_STEER_MOTOR = 0;
                BACK_LEFT_MODULE_STEER_ENCODER = 0;
                BACK_LEFT_MODULE_STEER_OFFSET = -Math.toRadians(0);

                BACK_RIGHT_MODULE_DRIVE_MOTOR = 0;
                BACK_RIGHT_MODULE_STEER_MOTOR = 0;
                BACK_RIGHT_MODULE_STEER_ENCODER = 0;
                BACK_RIGHT_MODULE_STEER_OFFSET = -Math.toRadians(0);
            }
        }
    }
        
    public static final class ShoulderConstants {
        public static final int SHOULDER_MOTOR_ID = 52;
        public static final double SHOULDER_MOTOR_SPEED = 0.6;
        public static final double SHOULDER_MOTOR_SPEED_SLOW = 0.6;
        public static final int SHOULDER_ENCODER_ID = 2;
        public static final double ENCODER_OFFSET = 0.627;
        
        // ENCODER ANGLES
        public static final double MAX_ENCODER_ANGLE = 0.334;
        public static final double MIN_ENCODER_ANGLE = 0.022;

        public static final double SLIDE_INTAKE_ENCODER_ANGLE = 0.148;
        public static final double GROUND_INTAKE_ENCODER_ANGLE = 0.299;
        public static final double CUBE_SPIT_ENCODER_ANGLE = 0.206;

        public static final double CONE_NODE_ENCODER_ANGLE = 0.046;
        
        public static final double HIGH_CUBE_ENCODER_ANGLE = 0.0759;
        public static final double MID_CUBE_ENCODER_ANGLE = 0.138;

        public static final double CUBE_INTAKE_BUMPER_ENCODER_ANGLE = 0.26;
        public static final double CUBE_INTAKE_SUCC_ENCODER_ANGLE = 0.28;


        // SPEEDS
        public static final double MAX_SPEED = 0.8;
        public static final double MIN_SPEED = 0.4;

        public static final double MIN_DOWN_SPEED = 0.2;
        public static final double MAX_DOWN_SPEED = 0.6;
    }


    public static final class ExtensionConstants {
        public static final int WINCH_MOTOR_ID = 51;
        public static final double WINCH_MOTOR_SPEED = .8;

        public static final int CONE_SLIDER_INTAKE_POS = 47823;
        public static final int CONE_UPPER_NODE_POS = 264000;
        public static final int CONE_MID_NODE_POS = 80000;       
        public static final int HIGH_CUBE_NODE_POS = 61000;
        public static final int CUBE_INTAKE_BUMPER_POS = 80000; // The extension at which we go to to get past bumpers 
        public static final int CUBE_INTAKE_POS = 74500; // The extension at which we intake cubes 

        public static final int MIN_EXTENSION_PAST_BUMPER_POS = 40000; // The minimum extension distance to be considered past bumpers (at low angles)
    }
    public static final class endEffectorConstants {
        public static final int ENDEFFECTOR_MOTOR_ID = 55;
        public static final double ENDEFFECTOR_MOTOR_SPEED = 0.8;
        public static final int ENDEFFECTOR_SOLENOIED1_ID1 = 1;
        public static final int ENDEFFECTOR_SOLENOIED1_ID2 = 0;
    }

    public static final class WristConstants {
        public static final int WRIST_MOTOR_ID = 54;
        public static final int ENCODER_ID = 1;
        public static final double ENCODER_OFFSET = 0.4683;
        public static final double WRIST_SPEED = 0.3;
    }
}

